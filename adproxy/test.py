# -*- coding: utf-8 *-*
""" Here is the testing facilities for emulating http request """

import socket

HTTP_REQUESTS = [
#"GET http://mail.google.com/mail/u/0/#inbox HTTP/1.1\r\n"\
#"User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/536.11 (KHTML, like Gecko) Ubuntu/12.04 Chromium/20.0.1132.47 Chrome/20.0.1132.47 Safari/536.11\r\n"\
#"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n\r\n",

"GET /ru/ HTTP/1.1\r\n"\
"Host: telemedia.ua\r\n"\
"Connection: keep-alive\r\n"\
"User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/536.11 (KHTML, like Gecko) Ubuntu/12.04 Chromium/20.0.1132.47 Chrome/20.0.1132.47 Safari/536.11\r\n"\
"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n"\
"Accept-Encoding: gzip,deflate,sdch\r\n"\
"Accept-Language: en-US,en;q=0.8\r\n"\
"Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.3\r\n"\
"Cookie: django_language=ru; livetex_visit=7; livetex_uid=15059037128e948354f5c952b02dd8fa66690681707c8eb893142a4c8778d43fa5cd4e555c; livetex_siteid=18139; livetex_countshow=1; livetex_beshow_16786=1; sessionid=e02ae167dbf3c125b553328d2bf52f4e; __utma=216387731.708992062.1351700346.1355172323.1355181542.9; __utmb=216387731.1.10.1355181542; __utmc=216387731; __utmz=216387731.1351700346.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); _ym_visorc=w\"\r\n\r\n"

# here may be other packets for testing
]

def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(('localhost', 8000))
    for request in HTTP_REQUESTS:
        sock.send(request)
        response = sock.recv(1024)
        print response, "/n/n"
