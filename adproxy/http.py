# -*- coding: utf-8 *-*
from BaseHTTPServer import BaseHTTPRequestHandler
from StringIO import StringIO


class HTTPRequest(BaseHTTPRequestHandler):
    " HTTP Request handler class"
    def __init__(self, conn):

        self.conn = conn
        self.raw_request_text = self.get_request_from_socket()
        self.rfile = StringIO(self.raw_request_text)
        self.raw_requestline = self.rfile.readline()
        self.error_code = self.error_message = None
        self.parse_request()

    def get_request_from_socket(self):
        http_pack = ''
        while 1:
            http_pack += self.conn.recv(1024)
            if http_pack.endswith('\r\n\r\n'):
                return http_pack

    def is_websocket(self):
        """
            Should determine whether this connection is attemting to open
            websocket or not
        """
        return False

    def send_error(self, code, message):
        self.error_code = code
        self.error_message = message

    def user_not_signed(self):
        """
            Check here if we have added cookies for this user
        """
        return True
