# -*- coding: utf-8 *-*

import socket
import httplib
import select
import logging

__version__ = '0.1.0 Draft 1'
BUFLEN = 8192
VERSION = 'Python Proxy/' + __version__
HTTPVER = 'HTTP/1.1'

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


class ProxyHandler(object):

    def __init__(self, connection, address, timeout=60):
        log.debug('Start handle new connection')
        self.client = connection
        self.client_buffer = ''
        self.timeout = timeout
        self.method, self.path, self.protocol = self.get_base_header()
        log.debug('%s %s %s' % (self.method, self.path, self.protocol))
        log.debug("Host: %s" % self.host)
        if self.is_websocket():
            self._handle_websocket()

        if self.method == 'CONNECT':
            self.method_CONNECT()
        elif self.method in ('OPTIONS', 'GET', 'HEAD', 'POST', 'PUT',
                             'DELETE', 'TRACE'):
            self.method_others()
        self.client.close()
        self.target.close()
        log.debug('End connection to %s' % self.path)

    def is_websocket(self):
        return self.client_buffer.find('Upgrade: websocket') != -1

    def get_base_header(self):
        self.get_headers_block()
        end = self.client_buffer.find('\n')
        data = (self.client_buffer[:end + 1]).split()
        self.client_buffer = self.client_buffer[end + 1:]
        # Get here Host name from the HTTP headers
        self._get_host()
        return data

    def get_headers_block(self):
        """ Reads header block from request and put it into the
        client buffer
        """
        while 1:
            self.client_buffer += self.client.recv(BUFLEN)
            end = self.client_buffer.find('\r\n\r\n')
            if end != -1:
                return

    def _get_host(self):
        begin = self.client_buffer.find('Host:')
        if begin != -1:
            # Headers contain Host
            end = self.client_buffer.find('\n', begin)
            header_host = self.client_buffer[begin:end]
            self.host = header_host.split()[1]
        else:
            # no - try to get it from path
            self.path = self.path[7:]
            i = self.path.find('/')
            self.host = self.path[:i]

    def method_CONNECT(self):
        self._connect_target(self.path)
        self.client.send(HTTPVER + ' 200 Connection established\n' +
                         'Proxy-agent: %s\n\n' % VERSION)
        self.client_buffer = ''
        self._read_write()

    def method_others(self):
        self._connect_target(self.host)
        self.target.send('%s %s %s\n' %
            (self.method, self.path, self.protocol) + self.client_buffer)
        self.client_buffer = ''
        self._read_write()

    def _connect_target(self, host):
        i = host.find(':')
        if i != -1:
            port = int(host[i + 1:])
            host = host[:i]
        else:
            port = 80
        (soc_family, _, _, _, address) = socket.getaddrinfo(host, port)[0]
        self.target = socket.socket(soc_family)
        self.target.connect(address)

    def _read_write(self):
        time_out_max = self.timeout / 3
        socs = [self.client, self.target]
        count = 0
        while 1:
            count += 1
            (recv, _, error) = select.select(socs, [], socs, 3)
            if error:
                break
            if recv:
                for in_ in recv:
                    data = in_.recv(BUFLEN)
                    if in_ is self.client:
                        out = self.target
                    else:
                        out = self.client
                    if data:
                        out.send(data)
                        count = 0
            if count == time_out_max:
                break

    def _handle_websocket(conn):
        "Handle websocket connection  just proxy it to server"
        return


def http_proxy(request):
    """
        This is proxying all requests from one socket to a real
        hosts and return response as HTTPResponse object
    """
    original_host = ''
    # first try to get Host from request
    if 'Host' in request.headers:
        original_host = request.headers['Host']
    else:
        # try here to get host from address string
        if request.path.startswith('http://'):
            path = request.path.replace('http://', '')
        else:
            path = request.path
        original_host = path.split('/')[0]

    # after we get original_host, try to get connection
    conn = httplib.HTTPConnection(original_host)
    conn.request(request.command, request.path, request.body, request.headers)

    return conn.getresponse()
