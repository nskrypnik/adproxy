# -*- coding: utf-8 *-*

from optparse import OptionParser
from twisted.web import proxy, http
from twisted.internet import reactor
from twisted.names.client import createResolver
from twisted.python import log
import urlparse
import sys


class AdProxyClient(proxy.ProxyClient):
    pass


class AdProxyClientFactory(proxy.ProxyClientFactory):

    protocol = AdProxyClient


class AdProxyRequest(proxy.ProxyRequest):

    protocol = AdProxyClientFactory

    def process(self):
        parsed = urlparse.urlparse(self.uri)
        protocol = parsed[0]
        host = parsed[1]
        port = 80
        headers = self.getAllHeaders()
        if not host:
            host = headers.get('host', '')
        if ':' in host:
            host, port = host.split(':')
            port = int(port)
        rest = urlparse.urlunparse(('', '') + parsed[2:])
        if not rest:
            rest = rest + '/'
        class_ = self.protocol
        headers = self.getAllHeaders().copy()
        if 'host' not in headers:
            headers['host'] = host
        self.content.seek(0, 0)
        s = self.content.read()
        clientFactory = class_(self.method, rest, self.clientproto, headers,
                               s, self)
        self.reactor.connectTCP(host, port, clientFactory)


class AdProxyProtocol(proxy.Proxy):

    requestFactory = AdProxyRequest


class ProxyFactory(http.HTTPFactory):
    protocol = AdProxyProtocol


def main():
    parser = OptionParser()
    parser.add_option("-p", "--port", dest="port")
    parser.add_option("-t", "--host", dest="host")

    options, args = parser.parse_args()
    host = options.host or '0.0.0.0'
    tcp_port = options.port or 80  # 80 port is default
    tcp_port = int(tcp_port)
    log.startLogging(sys.stdout)

    reactor.listenTCP(tcp_port, ProxyFactory())
    reactor.run()




