import os
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))

requires = []

setup(name='adproxy',
      version='0.1',
      author="Niko Skrypnik",
      packages=find_packages(where="."),
      package_dir={'': '.'},
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      entry_points="""
      [console_scripts]
      dnsserver = dnsserver.server:main
      adproxy = adproxy.server:main
      httptest = adproxy.test:main
      """
      )
